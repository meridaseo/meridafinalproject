import Framework7 from 'framework7/framework7.esm.bundle';
import $$ from 'dom7';
import firebase from 'firebase/app';
import app from "./F7App.js";
import 'firebase/database';
import 'firebase/auth';



$$("#tab2").on("tab:show", () => {
    //put in firebase ref here
    const sUser = firebase.auth().currentUser.uid;
    firebase.database().ref("weddingItems/" + sUser).on("value", (snapshot) => {
        const oItems = snapshot.val();
        const aKeys = Object.keys(oItems);
        $$("#weddingItemList").html("");
        for (let n = 0; n < aKeys.length; n++) {
            let sItem = "Venue shortlists :" + oItems[aKeys[n]].venue + "<br> Flowers : " + oItems[aKeys[n].flowers] + "<br> Photographers : " + oItems[aKeys[n].photo] + "<br> Cake : " + oItems[aKeys[n].cake];

            //checking for completed action
            if (oItems[aKeys[n]].datePurchased) {
                sItem = "<span class=\"completed\">" + sItem + "</span>";
            }

            //checking for completed action
            if (oItems[aKeys[n]].datePurchased) {
                sItem = "<span class=\"completed\">" + sItem + "</span>";
            }

            let sCard = sItem + `
            <div class="card">
                  <div class = "button button-active"><button id="${aKeys[n]}" type="submit" class= "update">I bought this</button></div><br>
                  <div class = "button button-active"><button id="${aKeys[n]}" type="submit" class= "delete">I don't need this</button></div>
                  </div>
                  </div>
            `
            $$("#weddingItemList").append(sCard);
        }

        deleteHandler();
        updateHandler();
    });

});

$$(".my-sheet").on("submit", e => {
    //submitting a new note
    e.preventDefault();
    const oData = app.form.convertToData("#addItem");
    const sUser = firebase.auth().currentUser.uid;
    const sId = new Date().toISOString().replace(".", "_");
    firebase.database().ref("weddingItems/" + sUser + "/" + sId).set(oData);
    app.sheet.close(".my-sheet", true);
});

function deleteHandler() {
    var aDeleteButtons = document.getElementsByClassName("delete");
    for (var i = 0; i < aDeleteButtons.length; i++) {
        aDeleteButtons[i].addEventListener("click", (evt) => {
            const sUser = firebase.auth().currentUser.uid;
            console.log("sUser:" + sUser);
            firebase.database().ref("weddingItems/" + sUser + "/" + evt.target.id).remove();//delete
        });
    }
}

function updateHandler() {
    var aUpdateButtons = document.getElementsByClassName("update");
    for (var i = 0; i < aUpdateButtons.length; i++) {
        aUpdateButtons[i].addEventListener("click", (evt) => {
            const sUser = firebase.auth().currentUser.uid;
            //console.log("sUser:"+ sUser);
            const sFinished = new Date().toISOString().replace(".", "_");
            firebase.database().ref("weddingItems/" + sUser + "/" + evt.target.id + "/datePurchased").set(sFinished);//update
        });
    }
}